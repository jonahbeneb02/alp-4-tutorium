---
theme: "metropolis"
aspectratio: 169
header-includes: \usepackage{amsmath}
---

# Tutorium 03

## Hausaufgabe

* Vorstellen

## Wiederholung

Ohne in die Vorlesung zu gucken überlegen:

- Was sind die (aktualisierten) Anforderungen zum Schutz des Kritischen Abschnitts?
- Finde zwei Dinge die sich zwischen dem alten und dem realen Maschinenmodell unterscheiden.
<!--  
The solution has to protect the critical section reliably by mutual exclusion.
• The solution has to protect the critical section reliably by mutual exclusion.
• The solution should be used in higher level programming languages.
    − Thus, the solution is usable on different architectures providing portability for the program using it.
• The solution must not lead to a deadlock.
• The solution should provide low overhead.
    − There is no (excessive) waiting in order to enter the critical section.
• The access to the critical section should be fair.
-->

## Petri-Netze

Welches der beiden Netze lebt?

![](netz_lebt.jpg)

## Petri-Netze

Warum können diese Transitionen nicht feuern?

![](feuern.jpg)

## Petri-Netze


![](kritischer_abschnitt.png)

## Petri-Netze

Visualisiert einen Softeisautomaten als Petri-Netz:

1. Der Automat wartet auf eine Münze.
1. Die Münze ist akzeptiert
1. Eis wird ausgegeben

Der Automat soll nur so lange Geld annehmen bis das Eis leer ist.

## Hausaufgabe 03/04

## Queue implementieren in C

**Stack**:

- Variable existiert nur in der aktuellen Funktion
- Zugriff ist schnell
- Stack-Größe ist begrenzt

**Heap**:

- Variable existiert bis sie gelöscht wird
- Speicher alloziieren mit `Struct *obj = malloc(sizeof(Struct))`
- Speicher freigeben mit `free(obj)`
- Erzeugen ist langsamer

## Queue implementieren in C

Vervollständigt die Datei `queue_template.c` bis das Programm durch läuft, und
```
1
Queue is empty
```
aus gibt.

Kompilieren mit `cc -o queue queue.c`
