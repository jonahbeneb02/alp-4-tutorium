#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct QueueEntry QueueEntry;

typedef struct QueueEntry {
    int elem;
    QueueEntry *next;
} QueueEntry;

typedef struct Queue {
    QueueEntry *first;
    QueueEntry *last;
} Queue;

/// Create an empty queue.
///
/// Ownership of the returned Queue * object is passed to the caller.
Queue *emptyQueue() {
    Queue *queue = malloc(sizeof(Queue));
    queue->first = NULL;
    queue->last = NULL;
    return queue;
}

/// Enqueue an integer
void queue_enqueue(Queue *queue, int elem) {
    QueueEntry *queue_block = malloc(sizeof(QueueEntry));
    queue_block->elem = elem;
    queue_block->next = NULL;
    if (!queue->first && !queue->last) {
        queue->first = queue_block;
        queue->last = queue_block;
    } else {
        queue->last->next = queue_block;
        queue->last = queue_block;
    }
}

/// Dequeue an integer.
///
/// The queue must not be empty.
/// The integer is returned.
int queue_dequeue(Queue *queue) {
    int elem = queue->first->elem;
    if (queue->first == queue->last) {
        free(queue->first);
        queue->first = NULL;
        queue->last = NULL;
        return elem;
    }

    QueueEntry *garbage = queue->first;
    queue->first = queue->first->next;
    free(garbage);
    return elem;
}

bool queue_empty(Queue *queue) {
    return !queue->first && !queue->last;
}

void queue_free(Queue *queue) {
    if (!queue->first || !queue->last) {
        return;
    }
    QueueEntry *to_free = NULL;
    for (QueueEntry *q = queue->first; q->next; q = q->next) {
        if (to_free) {
            free(to_free);
            to_free = NULL;
        }
        to_free = q;
    }
    if (to_free) {
        free(to_free);
    }
    free(queue);
}

int main() {
    Queue *queue = emptyQueue();
    queue_enqueue(queue, 1);
    queue_enqueue(queue, 4);
    int elem = queue_dequeue(queue);
    printf("%d\n", elem);
    queue_enqueue(queue, 3);
    queue_dequeue(queue);
    queue_dequeue(queue);
    bool empty = queue_empty(queue);
    if (empty) {
        printf("Queue is empty\n");
    } else {
        printf("Queue is not empty\n");
    }
    queue_free(queue);
}
