---
theme: metropolis
aspectratio: 169
---

# Klausurvorbereitung

## Bankers Algorithm

\begin{align*}
  \vec{v} = (5, 4, 5) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  2 & 0 & 1 \\
  0 & 1 & 3 \\
  3 & 0 & 1
  \end{pmatrix}
\end{align*}
\begin{align*}
  R = 
  \begin{pmatrix}
  0 & 1 & 1 \\
  0 & 1 & 0 \\
  2 & 0 & 3 \\
  \end{pmatrix}
\end{align*}

Können alle Threads beendet werden? Falls nicht, wie viele Resourcen werden zusätzlich benötigt?

## Bankers Algorithm

\begin{align*}
  \vec{f} = (1, 0, 3) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  4 & 4 & 4 \\
  1 & 3 & 3 \\
  2 & 3 & 1
  \end{pmatrix}
\end{align*}
\begin{align*}
  R = 
  \begin{pmatrix}
  4 & 1 & 2 \\
  0 & 3 & 0 \\
  1 & 0 & 3 \\
  \end{pmatrix}
\end{align*}

Können alle Threads beendet werden? Falls nicht, wie viele Resourcen werden zusätzlich benötigt?

## Bankers Algorithm

\begin{align*}
  \vec{f} = (1, 0, 3) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  4 & 4 & 4 \\
  1 & 3 & 3 \\
  2 & 3 & 1
  \end{pmatrix}
\end{align*}
\begin{align*}
  R = 
  \begin{pmatrix}
  4 & 1 & 2 \\
  0 & 3 & 0 \\
  1 & 0 & 3 \\
  \end{pmatrix}
\end{align*}

Können alle Threads beendet werden? Falls nicht, wie viele Resourcen werden zusätzlich benötigt?

## Bankers Algorithm

\begin{align*}
  \vec{f} = (1, 3, 0) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  1 & 0 & 1 \\
  0 & 0 & 2 \\
  2 & 5 & 2
  \end{pmatrix}
\end{align*}
\begin{align*}
  R = 
  \begin{pmatrix}
  2 & 1 & 5 \\
  1 & 0 & 0 \\
  1 & 0 & 3 \\
  \end{pmatrix}
\end{align*}

Können alle Threads beendet werden? Falls nicht, wie viele Resourcen werden zusätzlich benötigt?

## Petri Nets

Modelliere eine Produktionsstraße für Laptops in einem Petri-Netz.
Die verschiedenen Resourcentypen müssen nicht markiert werden, aber sollten als unterschiedlich angenommen werden, wenn sie nicht im gleichen Place verwendet werden.

## Locks

Welche Anforderungen an den Schutz des Kritischen Abschnittes erfüllt diese Lock-Implementierung?
```C
#define NUM_THREADS 2
int prio[2];

int lock(long tid) {
    prio[tid] = prio[NUM_THREADS - 1 - tid] + 1;
    
    while (prio[NUM_THREADS - 1 - tid] != 0 &&
        prio[NUM_THREADS - 1 - tid] < prio[tid]) {}
        
    return 0;
}
```

## Deadlocks

Kann es beim Problem der Speisenden Philosophen zu einem Deadlock kommen?
Wenn ja, welche Bedingungen sind erfüllt / nicht erfüllt?
Wenn nein, welche Bedingung ist notwendig aber nicht erfüllt?

## Determinismus

Kann ein Programm, dass Inhalte über ein Netzwerk liest noch deterministisch sein?

Der Zustand schließt hier explizit das Netzwerk mit ein.

## Map-Reduce

Was passiert im Schritt "Mapping" in Map-Reduce?
Grenze den Vorgang vom Schritt "Mapping" in Fosters-Design-Methode ab.

## Zeit

Warum muss man für einen Funktionierenden gegenseitigen Ausschluss über ein Verteiltes System hinweg die Zeit wissen?

## Zeit

Welche Zeit enthält mehr Informationen? Vektor-Zeit oder Reale-Zeit?
Durch welche Informationen unterscheiden sie sich?

## Maschinenmodell

Wodurch unterscheidet sich eine Atomare Operation von anderen CPU-Anweisungen?

## Maschinenmodell

Beschreibe das Maschinenmodell eines Clusters, das aus verschiedenen Rechnern besteht.

## Programmiermodell

Was kann passieren, wenn ein C-Programm uninitialisierten Speicher liest?

## Programmiermodell

Hat jedes Programm das kompiliert auch ein festgelegtes Verhalten im Programmiermodell?
Falls nicht, nenne ein Beispiel für eine Operation die kein festgelegtes Verhalten hat.

## Fosters-Design-Methode

Parallelisiere Lineare Suche mit Fosters-Design-Methode.

## Locking

Welche Vorteile ergeben sich, wenn der gegenseitige Ausschluss auf Betriebssystem-Ebene implementiert wird?

## Synchronisierung

Welchem anderen Konzept zur Synchronisierung entspricht eine Semaphore mit der Kapazit 1?

## Monitore

Welche der folgenden Dinge kann ein Monitor sein? Begründe.

- Betriebssystem-Kernel
- Webserver
- Datenbank-System

## Virtualisierung

Was kann alles virtualisiert werden? Nenne mindestens drei Dinge.

## Web-Programmierung

Welche Kriterien können verwendet werden, um die Architektur einer Webseite zu entscheiden?

## OpenMP und MPI

Wann sollte OpenMP, und wann MPI verwendet werden?

