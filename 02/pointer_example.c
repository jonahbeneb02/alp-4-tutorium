#include <stdio.h>
#include <stdlib.h>

#ifdef VERSION_1
//version 1
void minMax1(int arr[], int len, int min, int max)
{
	int i;
    for(i=1; i<len; i++)
    {
        if(arr[i]> max)
            max = arr[i];
        if(arr[i]< min)
            min = arr[i];
    }
}

#else

//version 2
void minMax2(int arr[], int len, int *min, int *max)
{
	int i;
	for(i=1; i<len; i++)
	{
		if(arr[i]> *max)
			*max = arr[i];
		if(arr[i]< *min)
			*min = arr[i];
	}
}

#endif

void example(int version)
{
	int a[] = {23,4,21,98,987,45,32,10,123,986,50,3,5};
	int min, max;
	min = max = a[0];
	int len = sizeof(a)/sizeof(a[0]);
	if(version == 1){
		minMax1(a, len, min, max);
	}else if(version == 2){
		minMax2(a, len, &min, &max);
	}else{
		printf("Unknown version %d\n", version);
		return;
	}
	printf("Minimum: %d and Maximum:%d\n", min, max);
}

int main(int argc, char const *argv[])
{
	(void) argv;
	(void) argc;
	int version = 1; 
	//get environment variable
	//use either export VERSION=1 or 2  before ./example
	//or execute program with: VERSION=2 ./example
	const char *v = getenv("VERSION");
	if ( v == NULL){
		printf("Environment variable not set, assuming version = 1\n");
	}else {
		version = atoi(v);
	}
	example(version);

	/* 
	// minor example
	int a = 2;
	int *ptr = &a;
	printf("%d\n",*ptr);
	printf("%d\n",ptr);
	*/
}
