---
theme: "metropolis"
aspectratio: 169
header-includes: \usepackage{amsmath}
---

# Tutorium 02

## Hausaufgabe

## Pointer in C

## Beispiel

## Wiederholung

Ohne in die Vorlesung zu gucken überlegen:

- Was ist der Unterschied zwischen deterministisch und determiniert?
- Was sind die Anforderungen zum Schutz des Kritischen Abschnitts?

Mit Sitznarbar\*in sammeln

## Expertengruppen

Was machen Compiler, Assembler und Linker?

## Diskussion

- Unterschied zwischen Thread und Prozess
- Können Prozesse noch deterministisch sein wenn mehrere parallel laufen?
- Was ist eine atomare Operation?

## In Gruppen

- Wie kann die verfügbare Rechenkapazität ausgenutzt werden?
  - Was für Probleme gibt es?

## In Gruppen

- Was macht den Systemzustand aus?
  - Woran kann man ihn ablesen?
