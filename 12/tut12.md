---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 12

## Klausurfragen

- Später ist noch Zeit für Klausurfragen

## Übungszettel

- Letzte Vorrechenpunkte sammeln
- Fragen zum 12. Übungszettel

## Matrix Time

- Bei jeder Nachricht wird das (von, zu)-Paar in eine Matrix eingetragen
- Matrix wird an nächste Nachricht angehängt
- Bei Empfang einer Nachricht:
    - für jedes Feld in der Matrix wird der größte bekannte Wert verwendet
    - Nachrichten warten bis die Vektorzeit nicht kleiner ist als die des Senders
    - Nachrichten werden in First-In-First-Out-Reihenfolge bearbeitet

## Imperative / Deklerative Frontend-Programmierung

**Imperativ**:
```rust
let title = "FU Berlin"

let obj = create_element("a")
obj.set_attribute("href", "https://fu-berlin.de")
obj.set_inner_html(title)
document.append_child(obj);
```

**Deklarativ**:
```
let title = "FU Berlin"

<a href="https://fu-berlin.de">{{ title }}</a>
```

## Server-Seitig HTML generieren

- Vorlagesprachen:

```jinja
<h1>Users</h1>

{% for user in users %}
<li>{{ user.name }}</li>
{% endfor %}
```

- Kontrollstrukturen:
    - Schleifen (for)
    - Bedingungen (if)
- Seite muss bei Änderungen der Variablen neu übertragen werden

## Client-Seitige Vorlagen

- gleiches Konzept, generiertes HTML muss aber nicht neu übertragen werden
- schnellere Aktualisierungen für kleinere Änderungen
- neue Variablen werden z.B. als JSON übertragen
- In der Regel integriert in JavaScript frameworks

## Java-Script Frameworks

- Beliebte Frameworks ändern sich relativ oft
- Langfristigkeit des Projetes sollte bei Auswahl beachtet werden
- Enthalten grötenteils Konzept von Komponenten die wiederverwendet werden können

## Web-Components

- In Browser integriertes Modell für wiederverwendbare Komponenten
- Erfordern kein JavaScript Framework

## Shadow DOM

- abgeschirmtes Stück DOM innerhalb einer Komponente

## Microservices / Monolith

**Microservices:**

Skalieren durch vervielfältigen spezifischer Teile die belastet sind.

**Monolith:**

Skalieren durch vervielfältigen des gesamten Services.

## Atomic Design

**Atom**: kleinste Einheit von UI Element. Kann nicht weiter aufgeteilt werden

**Molecules**: Einfache Kombinationen von Atoms

**Organisms**: Komplexe Elemente

## Übung

Wie könnte man die Teile eurer Webseite aus der Hausaufgabe einordnen?


## Bankers Algorithm

\begin{align*}
  \vec{v} = (6, 2, 2) 
\end{align*}
\begin{align*}
  B =
  \begin{pmatrix}
  2 & 1 & 0 \\
  3 & 1 & 0 \\
  0 & 0 & 1
  \end{pmatrix}
\end{align*}
\begin{align*}
  A = 
  \begin{pmatrix}
  1 & 1 & 0 \\
  1 & 0 & 1 \\
  2 & 0 & 1 \\
  \end{pmatrix}
\end{align*}

- Können alle Threads beendet werden?


## Übung zu Fosters Design Methology

Maximum in einer großen Liste von Elementen finden

## Klausurfragen
