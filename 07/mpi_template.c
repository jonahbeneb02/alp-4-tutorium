// simple program with MPI
// Rauber, Ruenger: Parallele und vert. Prg.

#include <stdio.h>
#include <string.h>
#include <mpi.h>

int main (int argc, char *argv[])
{
  int my_rank, p, source, dest, tag = 0;
  char msg[20];
  MPI_Status status;

  /* Initialisieren */
  /* Eigene ID herausfinden */
  /* Anzahl der Prozesse herausfinden */

  if (my_rank == 0) {
    strcpy (msg, "Hello ");
    /* Nachricht senden */ (msg, strlen (msg) + 1, MPI_CHAR, 1, tag, MPI_COMM_WORLD);
  }
  if (my_rank == 1)
    /* Nachricht empfangen */ (msg, 20, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &status);

  printf ("%d %s \n", my_rank, msg);

  MPI_Finalize ();

  return 0;
}
