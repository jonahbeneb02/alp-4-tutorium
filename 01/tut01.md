---
theme: "metropolis"
aspectratio: 169
header-includes: |
    \usepackage{amsmath}
---

## Kontakt

Jonah Brüchert

jonahbeneb02@zedat.fu-berlin.de

## Kriterien

- $n-1$ mal sinnvoll abgeben
- 2 mal eine Hausaufgabe präsentieren

## Abgabeworkflow

- Repo muss `npvp-exercises` heißen
- Branch Name musss `uebung-{Zahl} sein`
- Aufgabe muss mit `make run` ausführbar sein.
- Textaufgaben mit LaTeX als PDF (gerne mit dieser [Vorlage](https://www.overleaf.com/read/dbctyypwypqj#bc34c5))

## Warum Git

* Git wird nahezu überall in der Softwareentwicklung verwendet
* Es erlaubt mehreren Menschen strukturiert an den gleichen Dateien zu arbeiten
* Speichert Versionsverlauf, so dass Fehler zurückverfolgt werden können

## Repository anlegen

* Auf GitLab ein Projekt erstellen, dann

* ```bash
git clone https://git.imp.fu-berlin.de/{Benutzername}/npvp-exercises
cd npvp-exercises
git checkout -b main
```

* Tutor als Developer hinzufügen

\
\
Eine kleine Git-Anleitung findet ihr hier: [git.imp.fu-berlin.de/mactavish96/alp4-tutorials/-/blob/tutorial-1/git.md](https://git.imp.fu-berlin.de/mactavish96/alp4-tutorials/-/blob/tutorial-1/git.md)

## Abgeben

Wenn ihr unabhängig von einander an den Aufgaben arbeitet:

- Aktuellen Stand aus dem GitLab herunterladen: `git pull --rebase`

Dann

- Auf den "main" wechseln: `git checkout main` um ein leeres Arbeitsverzeichnis zu bekommen (wenn die Aufgabe nicht auf einer vorherigen aufbaut)
- einen neuen Branch von "main" abzweigen: `git checkout -b uebung-01`
- Aufgaben bearbeiten
- Dateien für die Abgabe vormerken: `git add Datei1.pdf Datei2.c`
- Einen Zwischenstand "Commit" erstellen: `git commit -m "Aufgabe 1 fertig"`
- Den aktuellen Stand hochladen: `git push`

## Vorlage für Makefile

[git.imp.fu-berlin.de/mielimom02/npvp-exercises](https://git.imp.fu-berlin.de/mielimom02/npvp-exercises/)

## Tutoriumsmaterial

Tutoriumsfolien und Beispiele werden immer einigermaßen zeitnah hier hochgeladen:
[git.imp.fu-berlin.de/jonahbeneb02/alp-4-tutorium](https://git.imp.fu-berlin.de/jonahbeneb02/alp-4-tutorium)

## Fragen zum Einrichten der Entwicklungsumgebung

Testet den Abgabeworkflow an eurer aktuellen Abgabe.
Wenn ihr noch nicht angefangen habt, verwendet eine leere Datei.

## Motivation

- Wofür braucht man nichtsequenzielle / parallele Programmierung?
- Welche Konzepte habt ihr schon mal gehört?

## Ausführung

Sucht euch zu zweit eine Programmiersprache die ihr kennt aus, und überlegt welche Schritte ihr kennt die zwischen Code und Ausführung passieren.

Beispiele:

- Python, C, C++, Haskell, Java, Scala.

## Wie geht ihr vor wenn ihr sicherstellen wollt dass euer Code funktioniert?

Gruppenarbeit

## Von welchen Eigenschaften der Ausführung geht ihr aus, wenn ihr ein Programm schreibt

Sammeln im Tutorium
