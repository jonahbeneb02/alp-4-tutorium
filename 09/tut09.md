---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 09

## Hausaufgabe

- Übung 7 vorstellen
- Übung 8 besprechen

## Map Reduce

1. Input Splitting
2. Mapping
3. Shuffling and Sorting
4. Reducing
5. Output

## Input Splitting

Die Eingabe wird zerteilt

## Mapping

Die eigentliche Logik wird ausgeführt.
Die Eingabe wird auf die gewollte Ausgabe abgebildet (daher Mapping).

## Shuffling and Sorting

Die Ergebnisse werden Schlüsseln zugeordnet, nach denen Sie im nächsten Schritt zusammengefügt werden können.

## Reducing

Die Ergebnisse werden nach den Schlüsseln zusammengefügt.

**Beispiel**: alle Listen mit dem key "ab1"
werden konkatiniert.

## Output

Ergebnis wird gespeichert

## Beispiel: Suchmaschine

Funktionsweise:

- verschiedene Rechner rufen Webseiten ab und indizieren sie
- Senden gefundene Wörter zurück
- bekommen im nächsten Schritt neue URLs zum abrufen.

Beispiel:
```json
{
    "FU": ["https://fu-berlin.de", "https://spline.de"],
    "Berlin": ["https://fu-berlin.de", "https://berlin.de"]
}
```

## Übung

- Teilt die verschiedenen Arbeitsschritte sauber auf, und ordnet sie den MapReduce-Schritten zu

## Virtualisierung

- Welche Aspekte von Infrastruktur kann man virtualisieren?

<!-- Netzertk, Rechner, Betriebssystem -->

## Overlay Networks

- Systeme haben eine IP-Adresse in der "physischen"-Umgebung
- Zusätzlich eine IP-Adresse in einem virtuellen Netzwerk
- Netzwerkverkehr von Virtueller Adresse wird über andere IP-Adresse zu anderen Rechnern im Virtuellen-Netzwerk geroutet

## Route über reales Netzwerk

```
$ traceroute to jbb.ghsq.de (62.144.243.238), 30 hops max, 60 byte packets
 1  furud.spline.inf.fu-berlin.de (130.133.110.193)  1.737 ms  1.536 ms  1.510 ms
 2  castor.imp.fu-berlin.de (160.45.113.244)  1.824 ms  1.846 ms  1.713 ms
10  cr-tub2-hundredgige0-2-0-0-3001.x-win.dfn.de (188.1.235.241)  4.161 ms  4.131 ms  4.114 ms
11  cr-erl2-be7.x-win.dfn.de (188.1.146.209)  13.377 ms  13.615 ms  13.451 ms
12  et-0-0-3-150.br02.r001.fra0.as12312.net (80.81.193.29)  14.792 ms  14.920 ms  14.897 ms
13  et-0-1-8.cr01.r106.fra0.as12312.net (62.27.97.25)  14.877 ms  14.760 ms  14.780 ms
15  62.27.94.110 (62.27.94.110)  14.562 ms  14.538 ms  14.453 ms
16  62.144.243.238 (62.144.243.238)  34.012 ms !X  33.995 ms !X  34.508 ms !X
```

## Route zum selben Rechner über virtueles Netzwerk

```
$ traceroute 10.10.8.3
traceroute to 10.10.8.3 (10.10.8.3), 30 hops max, 60 byte packets
 1  ixion.subspace (10.10.10.1)  32.490 ms  32.479 ms  32.465 ms
 2  rasputin.subspace (10.10.8.3)  79.481 ms * *
```

## Übung

- Wann macht es Sinn virtuelle Netzwerke einzusetzen?

## Virtuelle Maschinen (VMs)

- Welche Unterschiede gibt es bei Verschiedenen Umsetzungen?
    - Was wird Emuliert?
    - Was läuft in der Virtuellen Maschine?
    - Was sind die Hardwareanforderungen?
    
## Vorteile / Nachteile

- Welche Vor- und Nachteile kann Virtualisierung haben, und für wen?
