---
theme: "metropolis"
aspectratio: 169
---

# Tutorium 10

## Werbung

Bachelorbefragung

## Hausaufgabe

- Übungszettel 9 vorstellen
- Übungszettel 10 besprechen

## Document Object Model (DOM)

Interne Darstellung aller Webseiten im Browser

![](DOM.jpg)

## Rendering

bezogen auf Webseiten: Der Prozess aus Inhalten ein DOM zu erzeugen

- Client-Side: Browser bekommnt vom Server nur (z.B. JSON) und JavaScript mit Anweisungen wie daraus ein DOM erzeugt werden kann
- Server-Side: Browser bekommt ein fertiges HTML Dokument, dass nur noch geparst werden muss

## Rendering

verschiedene Abstufungen:

- Server-Side + JavaScript für kleine Interaktive Bereiche

## HTML + Beispiele

## Vor- und Nachteile

Client-Side: 

- Schnellere Reaktion auf Interaktion, da nicht immer neues HTML vom Server geladen werden muss
- komplexere Entwicklung -> Frameworks -> größere Webseiten -> längere Ladezeiten
- schlechtere Performance auf alten / billigen Geräten

Server-Side:

- unkompliziert
- relativ unflexibel

## Übung

Ordnet den Anwendungsfällen mögliche Rendering-Architekturen zu:

- Blog mit statischem Inhalt
- Messenger
- Seite mit Kochrezepten
- Online-Editor
- Social-Media-Webseite
- Nachrichtenseite

## HTTP

```bash
nc <domain> 80
```

```
GET / HTTP/1.1
Host: <domain>
```

## HTTPS

```bash
openssl s_client -host <domain> -port 443
```

```
GET / HTTP/1.1
Host: <domain>
```

## HTTP/2

- Binär-Protokoll (kleiner)
- Komprimierte Header
- Mehrere Anfragen Parallel über eine TCP Verbindung

## HTTP/3

- Multiplexing über TCP ersetzt durch UDP-Protokoll
- Verlorene Pakete blockieren nicht mehr alle Anfragen auf der Verbindung

## Übung

Ordnet den Fehlern den passenden Fehlercode zu:

- Dokument existiert nicht
- Man muss angemeldet sein um die Seite sehen zu können
- Ein Fehlerfall auf dem Server ist nicht behandelt
- Der Server ist überfordert
- Der Inhalt ist nur gegen Bezahlung zugreifbar

## Cookies

Übertragung über HTTP-Header

`Set-Cookie` Cookie auf Client setzen

`Cookie` Cookie in Anfrage wieder an den Server senden

## Übung

Wie könnte man damit Authentifizierung umsetzen?

## Reverse Proxy

- Webserver, der Anfragen

    1. entschlüsselt
    2. dekodiert
    3. (evtl. in anderer HTTP Version) wieder kodiert
    4. an einen anderen Webserver sendet

## Reverse Proxy Vorteile

- kleine Webserver (nodejs, Flask, Rocket) haben weniger widerstandsfähige HTTP Implementierungen
- durch reverse Proxy müssen sie nicht beliebige Anfragen aus dem Internet behandeln können
- verbreitete Probleme in Anwendungen:
    - keine Unterstützung neuer HTTP Versionen
    - keine Behandlung von Fehlerhaften Anfragen
    - keine Behandlung von DoS Techniken (viele Verbindungen öffnen, langsam Anfragen senden etc.)
